// $Id$

Date Repeat Formatter

*******************************************************************************
* DRUPAL 7.X NOTES
*******************************************************************************

  1) If updating from 6.x to 7.x you will have to reconfigure your formatters!
  
  2) There is now only one formatter, "Date repeat", instead of two. This 
     formatter has options to select "expanded" or "condensed" output,
     as well as the date format (Short, Long, etc.).
     
  3) Update your theme functions (see date_repeat_fm.theme.inc).

*******************************************************************************

This Drupal module provides a formatter, "Date repeat", for date fields 
(type 'date', 'datetime', 'datestamp', 'date_popup'). Its primary purpose is 
to tidy up and make more themeable the output when using repeating date 
functionality via the Date Repeat API.

"Condensed" vs "Expanded" output:

1.  Date repeat (option: Condensed)

    Variable output of a simple dateline.
    
    Examples (default output):
  
    A)  Sunday, January 2, 2011 - 14:00-16:00
        Standard date output.
    
    B)  Mon, 2011-02-21 01:00 (ended)
        Standard date output, date has passed.

    C)  Next event (Daily): Tue, 2011-03-01 02:00
        If a date with repeating frequency, will display the next event,
        with frequency, if it exists. If repeating in intervals will
        display (e.g.) "(Every 3 weeks)".
        
    D)  (Daily event) February 01, 2011 to February 09, 2011 (ended)
        A repeating date that has ended will display the repeat
        rule "(Daily event)" as well as the time period during which
        the event repeated.

2)  Date repeat (option: Expanded)

    Same as above, but appends a list of all repeating dates, hidden by 
    default, with a toggle view/hide link. This is the recommended formatter 
    for full node view

    Next event (Daily): Tue, 2011-03-01 02:00 | View all dates |
    -------- toggle show/hide on dates below ------> 
    All scheduled dates:
      Tue, 2011-03-01 02:00 (ended)
      Wed, 2011-03-02 02:00 (ended)
      Thu, 2011-03-03 02:00
      Fri, 2011-03-04 02:00

INSTALLATION
------------
     
Installing modules and themes:
http://drupal.org/documentation/install/modules-themes

SET-UP
------------

These instructions assume you have created a date field for a content type.

1.  Go to the "Display fields" page for your content type.
    e.g. /admin/structure/types/manage/[type-name]/display

2.  Choose "Date repeat" from the "Format" dropdown next to your date field. 
    
3.  Configure options (select "Condensed" or "Expanded", date format).
    
4.  Save.

THEMING
------------

To make the output accessible to themers, there are several theme functions which 
handle different "chunks" of output, such as the full listing of repeating dates,
the repeat rule and the "(ended)" string. 

See the theming guide for more info on theming with Drupal:
http://drupal.org/documentation/theme

