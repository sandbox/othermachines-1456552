<?php

/**
 * @file
 */

/**
 * Base theme function for condensed version of formatter.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_formatter_format_fdate($vars) {
  return theme('date_repeat_fm_dateline', array('date_info' => $vars['date_info']));
}

/**
 * Base theme function for expanded version of formatter.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_formatter_format_fdate_expanded($vars) {
  $output = theme('date_repeat_fm_dateline', array('date_info' => $vars['date_info']));
  $output .= theme('date_repeat_fm_view_all',  array('date_info' => $vars['date_info']));
  $output .= theme('date_repeat_fm_listing',  array('date_info' => $vars['date_info']));
  return $output;
}

/**
 * Theme conditional dateline output.
 *
 * Variable output depending on the following conditions:
 *   - Where the output should be abbreviated;
 *   - whether it is a repeating date;
 *   - whether the date has passed.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_dateline($vars) {

  $abbrev = $vars['abbrev'] ? $vars['abbrev'] : FALSE;
  
  $output = '';

  if ($vars['date_info']) {
    if ($abbrev) {
      if (!$vars['date_info']->repeating) {
        $output .= theme('date_repeat_fm_duration', array('date_info' => $vars['date_info'], 'index' => 0, 'abbrev' => TRUE));
        if ($vars['date_info']->ended) {
          $output .= theme('date_repeat_fm_ended');
        }
      }
      else {
        $output .= theme('date_repeat_fm_repeat_rule', array('date_info' => $vars['date_info']));
        if ($vars['date_info']->ended) {
          $output .= theme('date_repeat_fm_ended');
        }
        else {
          for ($i=0; $i < $vars['date_info']->num_items(); $i++) {
            if ($vars['date_info']->item_info($i, 'next')) {
              $output .= ' ' .  theme('date_repeat_fm_duration', array('date_info' => $vars['date_info'], 'index' => $i, 'abbrev' => TRUE));
              break;
            }
          }
        }
      }
    }
    else {
      if (!$vars['date_info']->repeating) {
        $output .= theme('date_repeat_fm_duration', array('date_info' => $vars['date_info']));
        if ($vars['date_info']->ended) {
          $output .= theme('date_repeat_fm_ended');
        }
      }
      else {
        if ($vars['date_info']->ended) {
          $output .= theme('date_repeat_fm_period', array('date_info' => $vars['date_info']));
          $output .= theme('date_repeat_fm_ended');
        }
        else {
          $i = 0;
          while ($i < $vars['date_info']->num_items()) {
            if ($vars['date_info']->item_info($i, 'next')) {
              $output .= theme('date_repeat_fm_next', array('date_info' => $vars['date_info']));
              break;
            }
            $i++;
          }
        }
      }
    }
  }

  return $output;
}

/**
 * Theme a standard date string, taking into account 'from' and 'to' dates.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_duration($vars) {

  $index = $vars['index'] ? $vars['index'] : 0;
  $abbrev = $vars['abbrev'] ? $vars['abbrev'] : FALSE;
  
  $output = '';

  global $language;

  $format = $abbrev ? 'abbrev' : 'formatted';

  if ($vars['date_info']) {
    
    $start = $vars['date_info']->item_info($index, 'start');
    $end = $vars['date_info']->item_info($index, 'end');
    
    $output = $start[$format];
    
    if ($end && !$abbrev) {
      // If 'start' and 'end' dates are the same day, but the time is different,
      // display time span rather than repeating both strings.
      if ( date('M-d-Y', $start['timestamp']) == date('M-d-Y', $end['timestamp']) ) {
        if ( $start['timestamp'] < $end['timestamp'] ) {
          if ($end['time']) {
            $output .= '-' . $end['time'];
          }
        }
      }
      else {
        $output = t('%date-from to %date-to', array('%date-from' => $output, '%date-to' => $end[$format]));
      }
    } 
    $output = '<span class="date_repeat_fm-duration' . ( $vars['date_info']->item_info($index, 'ended') ? ' past-date' : '' ) . '">' . $output . '</span>&nbsp;';
  }

  return $output;
}

/**
 * Theme a string showing frequency/interval when we have a repeating date.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_repeat_rule($vars) {
  
  $output = '';

  if ($vars['date_info'] && $vars['date_info']->repeating) {
    $freq_arr = array(
      'daily' => t('days'),
      'weekly' => t('weeks'),
      'monthly' => t('months'),
      'yearly' => t('years'),
      );

    $output = $vars['date_info']->interval
      ? t('Every %interval', array('%interval' => $vars['date_info']->interval . ' ' .  $freq_arr[$vars['date_info']->frequency]))
      : t('%frequency', array('%frequency' => drupal_ucfirst($vars['date_info']->frequency)));
    
    $output = '<span class="date_repeat_fm-frequency">' . $output . '</span>';
  }
  
  return $output;
}

/**
 * Repeating dates only; theme a string showing when the period begins and ends.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_period($vars) {
  
  $output = '';
  
  global $language;

  $output .= t('(!frequency event)', array('!frequency' => theme('date_repeat_fm_repeat_rule', array('date_info' => $vars['date_info'])))) . '&nbsp;';

  $start = $vars['date_info']->item_info(0, 'start');
  $end = $vars['date_info']->item_info($vars['date_info']->num_items()-1, 'end');
  
  if ($start && $end) {
    $output .= '<span class="date_repeat_fm-period">' . t('%start-date to %end-date', array('%start-date' => $start['mdy'], '%end-date' => $end['mdy'])) . '</span>&nbsp;';
  }
  return $output;
}

/**
 * Repeating dates only; theme a string showing the upcoming date.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_next($vars) {
  
  $output = '';
  
  $i = 0;
  while ($i < $vars['date_info']->num_items()) {
    $next = $vars['date_info']->item_info($i, 'next');
    if ($next) {
      $output .= '<span class="date_repeat_fm-next">' . t('Next event (!frequency): ', array('!frequency' => theme('date_repeat_fm_repeat_rule', array('date_info' => $vars['date_info'])))) . theme('date_repeat_fm_duration', array('date_info' => $vars['date_info'], 'index' => $i)) . '</span>';
      break;
    }
    $i++;
  }
  
  return $output;
}

/**
 * All dates; a string conveying that the date has passed.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_ended() {
  return '<span class="date_repeat_fm-ended">' . t('(ended)') . '</span>';
}

/**
 * Repeating dates only; display a link to a full list of dates.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_view_all($vars) {
  
  $output = '';
  
  if ($vars['date_info'] && $vars['date_info']->repeating) {
    $output .= '<span class="date_repeat_fm-view-all"><a href="#">' . t('View all dates') . '</a></span>';
  }
  
  return $output;
}

/**
 * Format a list of all dates.
 *
 * @ingroup themeable
 */
function theme_date_repeat_fm_listing($vars) {
  
  $output = '';

  if ($vars['date_info'] && $vars['date_info']->num_items() > 1) {
    $str = '';
    $i = 0;
    while ($i < $vars['date_info']->num_items()) {
      $str .= '<li>' . theme('date_repeat_fm_duration', array('date_info' => $vars['date_info'], 'index' => $i));
      $ended = $vars['date_info']->item_info($i, 'ended');
      if ($ended) {
        $str .= theme('date_repeat_fm_ended');
      }
      $str .= '</li>';
      $i++;
    }
    $output = '<div class="date_repeat_fm-listing"><p>' . t('All scheduled dates:') . '</p><ul>' . $str . '</ul></div>';
  }
  
  return $output;
}
